﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace armo
{
    public partial class Form1 : Form
    {
        bool ThreadPause;
        Thread th_search;
        Thread th_timer;
        Config Conf = Config.GetInstance();
        public Form1()
        {
            InitializeComponent();
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {

            IParams _params = new Params().Get(Conf.GetConfig("config.json"));

            th_search = new Thread(Start);
            th_timer = new Thread(TimerStart);

            textBox_param_directory.Text = _params.SearchDirectory;
            textBox_param_file_name.Text = _params.FileName;
            textBox_param_text_in_file.Text = _params.TextInFile;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            toolStripStatusLabel1.Text = "Search in process";         
            
            

            if (!th_search.IsAlive)
            {
                th_search = new Thread(Start);
                th_search.IsBackground = true;
                th_search.Start();
            }
                

            if (!th_timer.IsAlive)
            {
                th_timer = new Thread(TimerStart);
                th_timer.IsBackground = true;
                th_timer.Start();
            }

            ThreadPause = true;
        }

        private void TimerStart()
        {
            string dateFormat = "H:mm:ss";
            DateTime time = DateTime.Now;
            time = time.AddHours(-time.Hour).AddMinutes(-time.Minute).AddSeconds(-time.Second);

            while (th_timer.IsAlive)
            {
                while (ThreadPause)
                {
                    time = time.AddSeconds(1);
                    toolStripStatusLabel2.Text = time.ToString(dateFormat);
                    Thread.Sleep(1000);
                }                
            }
        }

        private void Start()
        {
           while (th_search.IsAlive)
           {
                while (ThreadPause)
                {
                    FileSearch.StartSearch(textBox_param_directory.Text,
                        textBox_param_file_name.Text,
                        textBox_param_text_in_file.Text);
                    toolStripStatusLabel3.Text = FileSearch.CurentFile;

                    foreach (SearchFile file in SearchFile.massiv)
                        treeView1.Invoke(new Action(() =>
                        {
                            foreach (TreeNode s in treeView1.Nodes)
                                if (s.Text == file.Value)
                                    return;
                            treeView1.Nodes.Add(file.Value);
                        }));

                    SearchBox.Invoke(new Action(() =>
                    {
                        SearchBox.Text = $"Search: {SearchFile.massiv.Count.ToString()}";
                    }));
                    Thread.Sleep(500);
                }
                
           }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            th_search.Abort();
            th_timer.Abort();
            treeView1.Nodes.Clear();
            toolStripStatusLabel3.Text = "";
            toolStripStatusLabel1.Text = "Search is stop!";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            toolStripStatusLabel1.Text = "Search is pause!";
            SearchBox.Text = "Search";
            ThreadPause = false;
            
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            
            if (ThreadState.Running == th_search.ThreadState)
                th_search.Abort();
            if (ThreadState.Running == th_timer.ThreadState)
                th_timer.Abort();

            Conf.SetConfig(textBox_param_directory.Text,
                    textBox_param_file_name.Text,
                    textBox_param_text_in_file.Text);
        }
    }
}
