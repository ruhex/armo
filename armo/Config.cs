﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace armo
{
    public class Config
    {
        private static Config _instance { get; set; }

        [JsonProperty("file_name")]
        public string FileName { get; set; }
        [JsonProperty("start_directory")]
        public string StartDirectory { get; set; }
        [JsonProperty("join_in_file")]
        public string JoinInFile { get; set; }

        public static Config GetInstance()
        {
            if (_instance == null)
                _instance = new Config();
            return _instance;
        }

        public Config GetConfig(string config_file_name)
        {
            string str = File.ReadAllText(config_file_name);
            return JsonConvert.DeserializeObject<Config>(str);
        }

        public void SetConfig(string search_directory, string file_name, string join_in_file)
        {
            this.FileName = file_name;
            this.JoinInFile = join_in_file;
            this.StartDirectory = search_directory;
            string str = JsonConvert.SerializeObject(this);
            File.WriteAllText("config.json", str);
        }


    }
}
