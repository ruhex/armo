﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace armo
{
    interface IParams
    {
        string SearchDirectory { get; set; }
        string FileName { get; set; }
        string TextInFile { get; set; }
    }
}
