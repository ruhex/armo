﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace armo
{
    class Params : IParams
    {
        public string SearchDirectory {get;set;}
        public string FileName { get; set; }
        public string TextInFile { get; set; }

        public Params Get(Config config)
        {
            this.SearchDirectory = config.StartDirectory;
            this.FileName = config.FileName;
            this.TextInFile = config.JoinInFile;
            return this;
        }
    }

}
