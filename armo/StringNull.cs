﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace armo
{
    static class StringNull
    {
        static public string GetNullTermSt(string str)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(str);
            sb.Append('\0');
            return sb.ToString();
        }
    }
}
