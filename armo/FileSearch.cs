﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Collections.Concurrent;

namespace armo
{
    class SearchFile
    {
        public static ConcurrentStack<SearchFile> massiv = new ConcurrentStack<SearchFile>();

        public readonly string Value;

        public SearchFile(string value)
        {
            if (value == null || value == "")
                throw new InvalidCastException(value);
            Value = value;
        }
    }

    static class FileSearch
    {
        public static string CurentFile {get;set;}

        public static void StartSearch(string directory, string file_name, string join_file)
        {

            try
            {
                SearchFile.massiv.Clear();
                {
                    string[] dirs = Directory.GetDirectories(@directory, "*", SearchOption.AllDirectories);
                    ConcurrentStack<string> _directorys = new ConcurrentStack<string>(dirs);
                    _directorys.Push(@directory);

                    foreach (string dir in _directorys)
                    {
                        string[] files = Directory.GetFiles(dir);

                        foreach (string file in files)
                        {
                            CurentFile = file;
                            FileInfo f = new FileInfo(file);
                            if (FindFineToName(f.Name, file_name) || FindInFile(file, join_file))
                                SearchFile.massiv.Push(new SearchFile(f.FullName));
                                
                            
                        }
                    }
                }
            }
            catch (Exception e)
            {
                //MessageBox.Show(e.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private static bool FindFineToName(string file, string file_name)
        {
            int i;
            int j;
            int p;

            p = 0;
            i = -1;

            file = StringNull.GetNullTermSt(file);
            file_name = StringNull.GetNullTermSt(file_name);

            if (file_name == "")
                return false;

            while (file_name[++i] != '\0')
            {
                j = -1;
                while (file[++j] != '\0')
                {
                    if (file_name[i] == file[j])
                    {
                        p++;
                    }
                        
                }
                //i++;
            }
            
            if (p == (file_name.Length - 1))
                return true;
            return false;

        }

        private static bool FindInFile(string file, string join_file)
        {
            int i;
            int j;
            int p;

            p = 0;
            i = -1;
            string[] strs = File.ReadAllLines(file);

            if (join_file == "")
                return false;

            foreach (string str in strs)
            {
                while (++i < join_file.Length && join_file.Length >= str.Length)
                {
                    j = 0;
                    while (j++ < str.Length)
                    {
                        if (join_file[i] == str[j])
                        {
                            i++;
                            p++;
                        }
                    }
                }
                if (p == (join_file.Length - 1))
                    return true;
            }
            return false;
        }
    }
}
